package com.jasonli.pintu;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.widget.ImageView;

public class PinTuBlock {

	public Bitmap mBitmap;
	public Point mCorrectPlace = new Point();
	public ImageView mImageView;
	private	boolean mIsComplete = false;
	
	public boolean isXPositionClose(int x) {
		return Math.abs(mCorrectPlace.x - x) < Utility.TOLERANCE;
	}
	public boolean isYPositionClose(int y) {
		return Math.abs(mCorrectPlace.y - y) < Utility.TOLERANCE;
	}
	
	public boolean isComplete() {
		return mIsComplete;
	}
	
	public void complete() {
		mIsComplete = true;
	}
	
	public void reset() {
		mIsComplete = false;
	}
}
