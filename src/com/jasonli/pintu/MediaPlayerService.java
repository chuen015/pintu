package com.jasonli.pintu;

import java.io.IOException;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

public class MediaPlayerService extends Service {
	public final static int MUSIC_PLAY_DEFAULT = 0;
	public final static int MUSIC_SET_URI = 1;
	public final static int MUSIC_PLAY = 10;
	public final static int MUSIC_PAUSE = 11;
	
    public static MediaPlayer mPlayer;
    
    @Override
    public void onCreate() {
        super.onCreate();
        if(mPlayer != null) {
        	mPlayer.reset();
        	mPlayer.release();
        	mPlayer = null;
        }
        
        mPlayer = new MediaPlayer();
        mPlayer.setLooping(true); // Set looping
        mPlayer.setVolume(50, 50);
        mPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer arg0) {
				mPlayer.start();
			}
		});
    }
    
    @Override
    public void onDestroy() {
        mPlayer.stop();
        mPlayer.release();
        mPlayer = null;
    }
    
	@Override
	public IBinder onBind(Intent arg0) {
		Log.w(Utility.TAG, "--- Service.onBind ");
		return null;
	}
	
	@Override  
    public int onStartCommand(Intent intent, int flags, int startId) {
		int command = intent.getIntExtra(Utility.KEY_PLAYER_SERVICE, MUSIC_PLAY_DEFAULT);
		Log.w(Utility.TAG, "--- Service.onStartCommand --- > " + command);
		switch(command) {
		case MUSIC_PLAY:
			if(Utility.mAudioUri == null) {
				// Play default
				Utility.mAudioUri = Uri.parse("android.resource://com.jasonli.pintu/" + R.raw.gaming);
			}
			playMusic(Utility.mAudioUri);
			break;
		default:
			break;
		}
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private void playMusic(Uri uri) {
		mPlayer.reset();
		setAudioUri(uri);

		try {
			mPlayer.prepare();
			mPlayer.start();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setAudioUri(Uri uri) {
		try {
			mPlayer.setDataSource(this, uri);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

}
