/**
 * Unused
 */

package com.jasonli.pintu.unused;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageLayout extends LinearLayout {
	private Context mContext;
	private ImageView[][] mImageViewArray;

	public ImageLayout(Context context, ImageView[][] imageViewArray){
		super(context);
		mImageViewArray = imageViewArray;
		mContext = context;
		setContent();
	}

	private void setContent() {
		LinearLayout linear = new LinearLayout(mContext);

		linear.setOrientation(LinearLayout.VERTICAL);
		
		for (int i = 0; i < mImageViewArray.length; i++) {
			// Put horizontal linearLayout first
			LinearLayout linearLayout = new LinearLayout(mContext);
			linearLayout.setOrientation(LinearLayout.HORIZONTAL);
						
			for (int j = 0; j < mImageViewArray[i].length; j++) {
				ImageView imageView = (ImageView) mImageViewArray[i][j];

				if (imageView != null) {
					linearLayout.addView(imageView, new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
				}
			}
			
			linear.addView(linearLayout);
		}
		
		this.addView(linear);
	}
	
	public ImageLayout(Context context, AttributeSet attrSet) {
		super(context, attrSet);
	}
	
	public ImageLayout(Context context, AttributeSet attrSet, int defStyle) {
		super(context, attrSet, defStyle);
	}
}