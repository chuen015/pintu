/**
 * Unused
 */

package com.jasonli.pintu.unused;

import com.jasonli.pintu.PinTuBlock;
import com.jasonli.pintu.R;
import com.jasonli.pintu.Utility;
import com.jasonli.pintu.R.id;
import com.jasonli.pintu.R.layout;

import android.app.Activity;
import android.content.ClipData;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;

public class AnswerActivity extends Activity {
	
	final String TAG = "GameActivity";
	final int NUM_LINES = 5;
	
	private LinearLayout mImagePreviewLayout;
	private ImageView mImageView;
	private Bitmap mCurrentBitmap = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		mImagePreviewLayout = (LinearLayout) findViewById(R.id.image_preview_layout);
		mImageView = (ImageView) findViewById(R.id.image_view_preview);
		
		// Load image from SDcard
//		String imageUriStr = getIntent().getExtras().getString(MainActivity.KEY_IMAGE_URI);
//		Uri imageUri = Uri.parse(imageUriStr);
//		mCurrentBitmap = Utility.getBitmapByUri(imageUri, getContentResolver());
//		mImageView.setImageBitmap(mCurrentBitmap);
		
		// Load image from bitmap cache (previous activity)
		byte[] imageBytes = getIntent().getExtras().getByteArray(Utility.KEY_IMAGE_BYTES);
		mCurrentBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		
		mImageSize = getIntent().getExtras().getInt(Utility.KEY_IMAGE_SIZE);
		
		setUpImageView();
		
		prepareImageContent();
		
		ImageLayout imageLayout = new ImageLayout(this, mImageViewArray);
		mImagePreviewLayout.addView(imageLayout);
	}
	
	int mImageSize;
		
	ImageView[][] mImageViewArray = new ImageView[NUM_LINES][NUM_LINES];
	PinTuBlock[][] mPinTuBlock = new PinTuBlock[NUM_LINES][NUM_LINES];
	
	private void prepareImageContent() {
		int blockSize = mCurrentBitmap.getWidth() / NUM_LINES;

		//Divide a bitmap to several image blocks
		for (int i = 0; i < NUM_LINES; i++) {
			for (int j = 0; j < NUM_LINES; j++) {
				int x = j * blockSize;
				int y = i * blockSize;

				Bitmap bitmap = Bitmap.createBitmap(mCurrentBitmap, x, y, blockSize, blockSize);
				mPinTuBlock[i][j] = new PinTuBlock();
//				mPinTuBlock[i][j].mCorrectPlace = j + ( j * i );
				mPinTuBlock[i][j].mBitmap = bitmap;
			}
		}
		
		//Show fragment in image content
		for (int i = 0; i < mPinTuBlock.length; i++){
			for (int j = 0; j < mPinTuBlock[i].length; j++) {
				ImageView imageView = new ImageView(this);
				imageView.setImageBitmap(mPinTuBlock[i][j].mBitmap);
				imageView.setScaleType(ScaleType.FIT_XY);
//				imageView.setOnClickListener(OnClickImageView);
				
				mImageViewArray[j][i] = imageView;
			}
		}
	}

	private void setUpImageView() {
		mImageView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				if (event.getAction() != MotionEvent.ACTION_DOWN) {
				    return false;
				}
				ClipData data = ClipData.newPlainText("", "");
				DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);

				v.startDrag(data, // the data to be dragged
						shadowBuilder, // the drag shadow builder
						null, // no need to use local data
						0 // flags (not currently used, set to 0)
				);
				return true;
			}
		});
		
		mImageView.setOnDragListener(new OnDragListener() {
			@Override
			public boolean onDrag(View v, DragEvent event) {
				Log.w(TAG, "Jason Drag!");
				
				android.widget.LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) v.getLayoutParams();
				switch (event.getAction()) {
				case DragEvent.ACTION_DRAG_STARTED:
					Log.d(TAG, "Action is DragEvent.ACTION_DRAG_STARTED");
					// Do nothing
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					Log.d(TAG, "Action is DragEvent.ACTION_DRAG_ENTERED");
					int x_cord = (int) event.getX();
					int y_cord = (int) event.getY();
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					Log.d(TAG, "Action is DragEvent.ACTION_DRAG_EXITED");
					x_cord = (int) event.getX();
					y_cord = (int) event.getY();
					layoutParams.leftMargin = x_cord;
					layoutParams.topMargin = y_cord;
					v.setLayoutParams(layoutParams);
					break;
				case DragEvent.ACTION_DRAG_LOCATION:
					Log.d(TAG, "Action is DragEvent.ACTION_DRAG_LOCATION");
					x_cord = (int) event.getX();
					y_cord = (int) event.getY();
					break;
				case DragEvent.ACTION_DRAG_ENDED:
					Log.d(TAG, "Action is DragEvent.ACTION_DRAG_ENDED");
					// Do nothing
					break;
				case DragEvent.ACTION_DROP:
					Log.d(TAG, "ACTION_DROP event");
					// Do nothing
					break;
				default:
					break;
				}
				return true;
			}
		});
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
	    super.onWindowFocusChanged(hasFocus);
	    Utility.adjustViewSize(mImagePreviewLayout);
	}

	@Override
	protected void onDestroy() {
		if(mCurrentBitmap != null)
			mCurrentBitmap.recycle();
		
		super.onDestroy();
	}

}
