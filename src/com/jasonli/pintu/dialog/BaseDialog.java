package com.jasonli.pintu.dialog;

import com.jasonli.pintu.R;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class BaseDialog extends Dialog {

	protected FrameLayout mContentLayout;
	protected Context mContext;

	public BaseDialog(Context context) {
		super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.setContentView(R.layout.dialog_base);
        
        /* make main activity light
        dialog.getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);*/
        
        Window window = getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);

        Button btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
        });

		mContext = context;
        mContentLayout = (FrameLayout) findViewById(R.id.dialogContent);
	}
	
	public void setContentView(int layoutId) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(layoutId, null);
        
        mContentLayout.addView(contentView);
	}

	public void setDialogTitle(String title) {
        TextView titleTextView = (TextView) findViewById(R.id.dialogTitleTextView);
        titleTextView.setText(title);
        titleTextView.getPaint().setFakeBoldText(true);
        titleTextView.setVisibility(View.VISIBLE);
	}

	public void setCancelText(String text) {
		Button btnCancel = (Button) findViewById(R.id.btn_cancel);
		btnCancel.setText(text);
	}
	
	public void setOKListener(View.OnClickListener listener) {
        Button btnConfirm = (Button) findViewById(R.id.btn_ok);
        btnConfirm.setOnClickListener(listener);
	}
	
	public void hideCancelButton() {
		Button btnCancel = (Button) findViewById(R.id.btn_cancel);
		btnCancel.setVisibility(View.GONE);
	}
	
	public void setCancelListener(View.OnClickListener listener) {
        Button btnCancel = (Button) findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(listener);
	}
}
