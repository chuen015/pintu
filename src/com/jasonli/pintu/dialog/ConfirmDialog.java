package com.jasonli.pintu.dialog;

import com.jasonli.pintu.R;

import android.content.Context;
import android.widget.TextView;

public class ConfirmDialog extends BaseDialog {

	public ConfirmDialog(Context context) {
		super(context);
        setContentView(R.layout.dialog_content_confirm);
	}
	
	public void setConfirmText(String text) {
        TextView customText = (TextView) findViewById(R.id.confirmText);
        customText.setText(text);
        customText.getPaint().setFakeBoldText(true);
	}

}
