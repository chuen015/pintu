package com.jasonli.pintu;

import java.util.Random;

import com.jasonli.pintu.dialog.ConfirmDialog;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GameActivity extends Activity {
	
	private LinearLayout mImagePreviewLayout;
	private Bitmap mCurrentBitmap = null;
	private ProgressBar mProgressBar;
	private TextView mProgressTextView;
	private int mGameProgress = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		mImagePreviewLayout = (LinearLayout) findViewById(R.id.image_preview_layout);
		mProgressTextView = (TextView) findViewById(R.id.textView_game_process);
		mProgressBar = (ProgressBar) findViewById(R.id.progressBar_game_process);
		setGameProgress();
		
		// Load image from bitmap cache (previous activity)
		byte[] imageBytes = getIntent().getExtras().getByteArray(Utility.KEY_IMAGE_BYTES);
		mCurrentBitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
		
		mImageSize = getIntent().getExtras().getInt(Utility.KEY_IMAGE_SIZE);
		
		prepareImageContent();
		
		randomize();
		
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	private int mImageSize;
	private int mBlockSize;
	
	private PinTuBlock[][] mPinTuBlock = new PinTuBlock[Utility.NUM_LINES][Utility.NUM_LINES];
	
	private void prepareImageContent() {
		mBlockSize = mCurrentBitmap.getWidth() / Utility.NUM_LINES;

		// Divide a bitmap to several image blocks
		for (int i = 0; i < Utility.NUM_LINES; i++) {
			for (int j = 0; j < Utility.NUM_LINES; j++) {
				int x = j * mBlockSize;
				int y = i * mBlockSize;

				Bitmap bitmap = Bitmap.createBitmap(mCurrentBitmap, x, y, mBlockSize, mBlockSize);
				mPinTuBlock[i][j] = new PinTuBlock();
				mPinTuBlock[i][j].mCorrectPlace.x = Utility.getCorrectX(i, j, Utility.mOriginPoint.x, Utility.mOriginPoint.y, mBlockSize);
				mPinTuBlock[i][j].mCorrectPlace.y = Utility.getCorrectY(i, j, Utility.mOriginPoint.x, Utility.mOriginPoint.y, mBlockSize);
				mPinTuBlock[i][j].mBitmap = bitmap;

				// Transfer bitmap into imageView
				ImageView imageView = new ImageView(this);
				imageView.setImageBitmap(mPinTuBlock[i][j].mBitmap);
				imageView.setScaleType(ScaleType.FIT_XY);
				mPinTuBlock[i][j].mImageView = imageView;
			}
		}
	}
	
	private void randomize() {
		RelativeLayout rootLayout = (RelativeLayout) mImagePreviewLayout.getParent();
		for (int i = 0; i < mPinTuBlock.length; i++) {
			for (int j = 0; j < mPinTuBlock[i].length; j++) {
				ImageView imageView = mPinTuBlock[i][j].mImageView;
				
				RelativeLayout.LayoutParams params;
				params = new RelativeLayout.LayoutParams(mBlockSize, mBlockSize);
				params.leftMargin = 0;
				params.topMargin = 0;
				imageView.setOnTouchListener(mImageTouchListener);
				rootLayout.addView(imageView, params);
			}
		}
		
		rootLayout.requestLayout();
	}
	
	Point mImagePreviewLayoutPoint;
	Point mRootLayoutPoint;
	Point mWindowSize;

	private View.OnTouchListener mImageTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent event) {
			PinTuBlock block = getPickBlock(view);
			
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				view.bringToFront();
				break;
			case MotionEvent.ACTION_UP:
				int x_screen = (int) event.getRawX() - mImagePreviewLayoutPoint.x - mBlockSize/2;
				int y_screen = (int) event.getRawY() - mImagePreviewLayoutPoint.y - mBlockSize/2 + mRootLayoutPoint.y;
//				Log.w(Utility.TAG, "Correct Point = [" + (block.mCorrectPlace.x) + ", " + (block.mCorrectPlace.y) + "]");
//				Log.i(Utility.TAG, "Up Point = [" + (x_screen) + ", " + (y_screen) + "]");
				
				if(block.isXPositionClose(x_screen) && block.isYPositionClose(y_screen) && !block.isComplete()) {
					RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
					layoutParams.leftMargin = block.mCorrectPlace.x;
					layoutParams.topMargin = block.mCorrectPlace.y - mRootLayoutPoint.y;
					view.setLayoutParams(layoutParams);
					block.complete();
					mGameProgress++;
					setGameProgress();
					checkIfGameFinished();
					view.setEnabled(false);
//					moveToBack(view);
				}
				
				break;
			case MotionEvent.ACTION_MOVE:
				if(block.isComplete())
					break;
				
				int x_cord = (int) event.getRawX() - mImagePreviewLayoutPoint.x;
                int y_cord = (int) event.getRawY() - mImagePreviewLayoutPoint.y;
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();

                if(x_cord<0){x_cord=0;}
                if(y_cord<0){y_cord=0;}
                if(x_cord>mWindowSize.x){x_cord=mWindowSize.x;}
                if(y_cord>mWindowSize.y){y_cord=mWindowSize.y;}

                layoutParams.leftMargin = x_cord - mBlockSize/2;
                layoutParams.topMargin = y_cord - mBlockSize/2;

                view.setLayoutParams(layoutParams);
				break;
			default:
				return false;
			}
			return true;
		}
	};
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
	    super.onWindowFocusChanged(hasFocus);
	    Utility.adjustViewSize(mImagePreviewLayout);

	    mImagePreviewLayout.getLayoutParams().width = mImageSize;
	    mImagePreviewLayout.getLayoutParams().height = mImageSize;
	    mImagePreviewLayout.requestLayout();
	    
	    // First layout finished
	    if(mImagePreviewLayoutPoint == null) {
	    	mImagePreviewLayoutPoint = new Point();
			int[] point = new int[2];
			mImagePreviewLayout.getLocationOnScreen(point);
			mImagePreviewLayoutPoint.x = point[0];
			mImagePreviewLayoutPoint.y = point[1];
			Log.w(Utility.TAG, "mImagePreviewLayoutPoint (x, y) = ("+point[0]+", "+point[1]+")");
			
			mRootLayoutPoint = new Point();
			((View) mImagePreviewLayout.getParent()).getLocationOnScreen(point);
			mRootLayoutPoint.x = point[0];
			mRootLayoutPoint.y = point[1];
			Log.w(Utility.TAG, "mRootLayoutPoint (x, y) = ("+point[0]+", "+point[1]+")");
			
			Display display = getWindowManager().getDefaultDisplay();
			mWindowSize = new Point();
			display.getSize(mWindowSize);
			Log.w(Utility.TAG, "mWindowSize (w, h) = ("+mWindowSize.x+", "+mWindowSize.y+")");
			
			resetGame();
	    }
	}
	
	private void moveToBack(View myCurrentView) {
//        ViewGroup myViewGroup = ((ViewGroup) myCurrentView.getParent());
//        int index = myViewGroup.indexOfChild(myCurrentView);
//        for(int i = 0; i < index; i++) {
//            myViewGroup.bringChildToFront(myViewGroup.getChildAt(i));
//        }
    }
	
	private void checkIfGameFinished() {
		for (int i = 0; i < mPinTuBlock.length; i++) {
			for (int j = 0; j < mPinTuBlock[i].length; j++) {
				if(!mPinTuBlock[j][i].isComplete()) {
					return;
				}
			}
		}
		
		// Game finished!
		final ConfirmDialog dialog = new ConfirmDialog(GameActivity.this);
		dialog.setConfirmText(this.getString(R.string.finish_game));
		dialog.setCancelText(this.getString(R.string.restart));
		dialog.setCancelListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				resetGame();
				dialog.dismiss();
			}
		});
		dialog.setOKListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GameActivity.this.finish();
			}
		});
		dialog.show();
	}
	
	private void resetGame() {
		// Random put blocks on screen
		for (int i = 0; i < mPinTuBlock.length; i++) {
			for (int j = 0; j < mPinTuBlock[i].length; j++) {
				ImageView imageView = mPinTuBlock[j][i].mImageView;
				Random rand = new Random();
				RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
				layoutParams.leftMargin = rand.nextInt(mWindowSize.x - mBlockSize) + 1;
				layoutParams.topMargin = rand.nextInt(mWindowSize.y / 3) + (mWindowSize.y / 3) + mRootLayoutPoint.y;

				imageView.setLayoutParams(layoutParams);
				
				mPinTuBlock[j][i].reset();
				mGameProgress = 0;
				setGameProgress();
			}
		}
	}
	
	private void setGameProgress() {
		int total = Utility.NUM_LINES * Utility.NUM_LINES;
		mProgressTextView.setText(mGameProgress + "/" + total);
		mProgressBar.setMax(total);
		mProgressBar.setProgress(mGameProgress);
	}
	
	private PinTuBlock getPickBlock(View view) {
		for (int i = 0; i < mPinTuBlock.length; i++) {
			for (int j = 0; j < mPinTuBlock[i].length; j++) {
				ImageView imageView = mPinTuBlock[j][i].mImageView;
				if(imageView == view) 
					return mPinTuBlock[j][i];
			}
		}
		return null;
	}

	@Override
	protected void onDestroy() {
		if(mCurrentBitmap != null)
			mCurrentBitmap.recycle();
		
		super.onDestroy();
	}
	
	@Override 
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_restart:
			resetGame();
			return true;
		case android.R.id.home:
            // app icon in action bar clicked; go home 
            GameActivity.this.finish();
            return true; 
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override 
	public boolean onCreateOptionsMenu(Menu menu) { 
	    MenuInflater inflater = getMenuInflater(); 
	    inflater.inflate(R.menu.game_menu, menu); 
	    return true; 
	}

}
