package com.jasonli.pintu;

import java.io.ByteArrayOutputStream;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private final int REQ_CODE_PICK_IMAGE = 0;
	private final int REQ_CODE_PICK_AUDIO = 1;
	
	private final String PREF_NAME = "pintu";
	private final String PINTI_SIZE = "pintu_size";
	
	private LinearLayout mImagePreviewLayout;
	private ImageView mPreviewImage;
	private Button mButtonSelectPic;
	private Button mButtonSelectMusic;
	private Button mButtonStart;
	private ImageButton mButtonPlayMusic;
	private SeekBar mSeekBarSize;
	private TextView mTextViewSize;
	
	private SharedPreferences mPref;
	
	private int mImageSize = 0;
	private Uri mImageUri = null;
	private Uri mAudioUri = null;
	private Bitmap mCurrentBitmap = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_layout);
		
		mPref = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
		
		mImagePreviewLayout = (LinearLayout) findViewById(R.id.image_preview_layout);
		mPreviewImage = (ImageView) findViewById(R.id.image_view_preview);

		mTextViewSize = (TextView)findViewById(R.id.textView_size);
		mSeekBarSize = (SeekBar)findViewById(R.id.seekBar_size);
		mSeekBarSize.setMax(Utility.TYPE_NUM);
		int pintuSize = mPref.getInt(PINTI_SIZE, 5);
		Utility.setPinTuSize(pintuSize);
		setSizeText(pintuSize);
		mSeekBarSize.setProgress(pintuSize - Utility.TYPE_START);
		mSeekBarSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				int seekProgress = seekBar.getProgress();
				setPinTuSize(seekProgress);
				setSizeText(seekProgress + Utility.TYPE_START);
			}
		});
		
		mButtonSelectPic = (Button) findViewById(R.id.button_select_pic);
		mButtonSelectPic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);   
			}
		});
		
		mButtonSelectMusic = (Button) findViewById(R.id.button_select_music);
		mButtonSelectMusic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
		        CharSequence selectMusicText =  MainActivity.this.getResources().getText(R.string.select_music);
		        startActivityForResult(Intent.createChooser(intent, selectMusicText), REQ_CODE_PICK_AUDIO);
			}
		});
		
		mButtonPlayMusic = (ImageButton) findViewById(R.id.button_play_music);
		mButtonPlayMusic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(Utility.mAudioUri == null) {
					commandMusic(MediaPlayerService.MUSIC_PLAY);
					mButtonPlayMusic.setImageResource(android.R.drawable.ic_media_pause);
					return;
				}
				
				if(!MediaPlayerService.mPlayer.isPlaying()) {
					MediaPlayerService.mPlayer.start();
					mButtonPlayMusic.setImageResource(android.R.drawable.ic_media_pause);
				} else {
					MediaPlayerService.mPlayer.pause();
					mButtonPlayMusic.setImageResource(android.R.drawable.ic_media_play);
				}
			}
		});
		
		mButtonStart = (Button) findViewById(R.id.button_start_game);
		mButtonStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, GameActivity.class);
				
				// Refresh drawing cache every time
				mPreviewImage.setDrawingCacheEnabled(true);
				mPreviewImage.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
						MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
				Point point = new Point();
				int[] ivLocation = new int[2];
				mPreviewImage.getLocationOnScreen(ivLocation);
				point.x = ivLocation[0];
				point.y = ivLocation[1];
				Log.w(Utility.TAG, "mPreviewImage.screen point = (" + point.x + ", " + point.y + ")");
				Utility.mOriginPoint = point;
				
				// get cache of the image on screen
				mPreviewImage.buildDrawingCache();
				Bitmap bm = mPreviewImage.getDrawingCache();
				ByteArrayOutputStream bs = new ByteArrayOutputStream();

				if(bm == null) {
					Log.e(Utility.TAG, "mImageView.getDrawingCache = null !");
					return;
				}
				bm.compress(Bitmap.CompressFormat.JPEG, 100, bs);
				intent.putExtra(Utility.KEY_IMAGE_BYTES, bs.toByteArray());
				intent.putExtra(Utility.KEY_IMAGE_SIZE, mImageSize);
				
				mPreviewImage.setDrawingCacheEnabled(false);
				
				startActivity(intent);
			}
		});
	}
	
	private void commandMusic(int action) {
		Intent intent = new Intent();
        intent.putExtra(Utility.KEY_PLAYER_SERVICE, action);
        intent.setClass(MainActivity.this, MediaPlayerService.class);          
        startService(intent);
	}
	
	private void setPinTuSize(int size) {
		size += Utility.TYPE_START;
		Utility.setPinTuSize(size);
		mPref.edit().putInt(PINTI_SIZE, size).commit();
	}
	
	private void setSizeText(int size) {
		mTextViewSize.setText(size + "��" + size);
	}
	
	private void setPreviewImage(Bitmap bitmap) {
		mPreviewImage.setMinimumHeight(mImageSize);
		mPreviewImage.setMinimumWidth(mImageSize);
		mPreviewImage.setImageBitmap(bitmap);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

		if(resultCode != RESULT_OK)
			return;
		
		switch (requestCode) {
		case REQ_CODE_PICK_IMAGE:
			mImageUri = imageReturnedIntent.getData();

			if (mCurrentBitmap != null) {
				mCurrentBitmap.recycle();
			}
			mCurrentBitmap = Utility.getBitmapByUri(mImageUri, getContentResolver());

			mImageSize = Utility.adjustViewSize(mImagePreviewLayout);
			Log.w(Utility.TAG, "mImageSize size :" + mImageSize);
			Log.w(Utility.TAG, "Get image URI from SDCard :" + mImageUri.toString());
			setPreviewImage(mCurrentBitmap);

			mButtonStart.setEnabled(true);
			break;
		case REQ_CODE_PICK_AUDIO:
			mAudioUri = imageReturnedIntent.getData();
			Utility.mAudioUri = mAudioUri;
			Log.w(Utility.TAG, "Get audio URI from SDCard :" + mAudioUri.toString());
			commandMusic(MediaPlayerService.MUSIC_PLAY);
			mButtonPlayMusic.setVisibility(View.VISIBLE);
			mButtonPlayMusic.setImageResource(android.R.drawable.ic_media_pause);
			
//			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//			Intent notifyIntent = new Intent(MainActivity.this, MainActivity.class);
//			notifyIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
//			PendingIntent appIntent = PendingIntent.getActivity(MainActivity.this, 0, notifyIntent, 0);
//			Notification notification = new Notification();
//			notification.icon = R.drawable.ic_launcher;
//			notification.tickerText = "Press to stop";
//			notification.flags |= Notification.FLAG_AUTO_CANCEL;
//			notification.setLatestEventInfo(MainActivity.this, "Title", "Press to stop", appIntent);
//			notificationManager.notify(0,notification);
			break;
		}
	}
	
	@Override
	protected void onDestroy() {
		if(mCurrentBitmap != null)
			mCurrentBitmap.recycle();
		
		mPreviewImage.destroyDrawingCache();
		Intent intent = new Intent(MainActivity.this, MediaPlayerService.class);
		stopService(intent);
		Utility.mAudioUri = null;

		super.onDestroy();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
	    super.onWindowFocusChanged(hasFocus);
	    mImageSize = Utility.adjustViewSize(mImagePreviewLayout);
	    mImagePreviewLayout.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
