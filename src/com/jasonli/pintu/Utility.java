package com.jasonli.pintu;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;

public class Utility {
	public final static String KEY_IMAGE_URI = "image_uri";
	public final static String KEY_IMAGE_BYTES = "image_bytes";
	public final static String KEY_IMAGE_SIZE = "image_size";
	
	public final static String KEY_PLAYER_SERVICE = "player_service";
	
	public final static String TAG = "Jason";
	public final static int TOLERANCE = 40;
	public static int NUM_LINES = 5;
	public static int TYPE_NUM = 8;
	public static int TYPE_START = 2;
	public static Uri mAudioUri = null;
	
	private final static int BITMAP_SAMPLE_SIZE = 2;
	
	public static Point mOriginPoint = null;
	
	public static void setPinTuSize(int size) {
		NUM_LINES = size;
	}
	
	public static Bitmap getBitmapByUri(Uri uri, ContentResolver resolver) {
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = resolver.query(uri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String filePath = cursor.getString(columnIndex);
		cursor.close();
		
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = BITMAP_SAMPLE_SIZE;
		
		return BitmapFactory.decodeFile(filePath, opts);
	}
	
	public static int adjustViewSize(View layout) {
		int height = layout.getHeight();
		int width = layout.getWidth();
		
		if (height < width) {
			layout.getLayoutParams().width = height;
		}
		if (height > width) {
			layout.getLayoutParams().height = width;
		}
		layout.requestLayout();
		
		return height;
	}
		
	public static int getCorrectX(int i, int j, int offsetX, int offsetY, int blockSize) {
		return j * blockSize + offsetX;
	}
	
	public static int getCorrectY(int i, int j, int offsetX, int offsetY, int blockSize) {
		return i * blockSize + offsetY;
	}
}
